## Angular Service Redirect / Bravoure component

This Component sets the ajax call to make when the page is not found (potential 404) to decide if it should be redirected or not

### **Versions:**

1.0.1 - Added redirection with '/' and replacing the history
1.0.0 - Initial Stable version

---------------------
