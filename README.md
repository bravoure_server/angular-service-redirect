# Bravoure - Service Redirect

## Use

This Component sets the ajax call to make when the page is not found (potential 404) to decide if it should be redirected or not.

### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-service-redirect": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
