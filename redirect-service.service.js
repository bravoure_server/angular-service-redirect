(function () {

    'use strict';

    function redirectService (APP_CONFIG, $location, PATH_CONFIG, API_Service, $rootScope, $state) {

        return {

            redirectCall: function () {

                var i18n = APP_CONFIG[1]['content']['i18n'];

                var selectedLangRedirect = (i18n.indexOf(browserLang) -1 ) ? browserLang : PATH_CONFIG.current_language ;
                // Prerender redirection & 404 check if the page should be redirected

                var formData = {
                    "uri": $location.$$url,
                    "language": selectedLangRedirect
                };

                API_Service(host + 'api/redirect', {}).save_no_array({}, formData,
                    function success(data) {

                        if (data.http_status_code == 301) {
                            // Sets the Prerender Status Code to 301

                            $rootScope.preRenderStatus = 301;
                            $rootScope.preRender301Status = true;
                            $rootScope.preRenderHeader = 'Location: ' + $location.$$protocol + '://' + $location.$$host + data.uri;

                            // Don't execute redirect for Prerender
                            if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {

                                // Checks if the language is the same of the application,
                                // if not it hard reloads the page with the correct uri
                                var lang = data.uri.split('/');

                                if (lang[1] == PATH_CONFIG.current_language) {
                                    $location.path(data.uri);
                                } else {
                                    // hard reload of the page with correct language, also replaces the history
                                    location.replace(data.uri);
                                }
                            }
                        }

                        if (data.http_status_code == 302) {
                            // Sets the Prerender Status Code to 302

                            $rootScope.preRenderStatus = 302;
                            $rootScope.preRender302Status = true;
                            $rootScope.preRenderHeader = 'Location: ' + $location.$$protocol + '://' + $location.$$host + data.uri;

                            // Don't execute redirect for Prerender
                            if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {

                                // Checks if the language is the same of the application,
                                // if not it hard reloads the page with the correct uri
                                var lang = data.uri.split('/');

                                if (lang[1] == PATH_CONFIG.current_language) {
                                    $location.path(data.uri);
                                } else {
                                    // hard reload of the page with correct language, also replaces the history
                                    location.replace(data.uri);
                                }
                            }
                        }

                        if (data.http_status_code == 404) {

                            var path = $location.$$path;

                            // add Trailing Slash
                            if (path[path.length-1] != '/') {

                                $rootScope.preRenderStatus = 301;
                                $rootScope.preRender301Status = true;
                                $rootScope.preRenderHeader = 'Location: ' + $location.$$absUrl + '/';

                                // Don't execute redirect for Prerender
                                if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {

                                    // Redirection with '/' and replacing the history
                                    location.replace(path + '/');
                                }

                            } else {

                                // redirects to the 404 page
                                $rootScope.preRenderStatus = 404;

                                // Don't execute redirect for Prerender
                                if (navigator.userAgent.toLowerCase().indexOf('prerender') == -1) {

                                    $state.go(PATH_CONFIG.ERROR_IDENTIFIER, null, {
                                        location: false
                                    });

                                }
                            }
                        }
                    },
                    function error(data) {

                        // redirects to the 404 page
                        $rootScope.preRenderStatus = 404;
                        $state.go(PATH_CONFIG.ERROR_IDENTIFIER, null, {
                            location: false
                        });
                    });

            }
        }
    }


    redirectService.$inject = [
        'APP_CONFIG', '$location', 'PATH_CONFIG', 'API_Service', '$rootScope', '$state'
    ];

    angular
        .module('bravoureAngularApp')
        .factory('redirectService', redirectService);


})();
